#include "List.h"



int ll_add(List* l, void* value){
	((struct LinkedList*) (l->f_el))->value = value;
	((struct LinkedList*) (l->f_el))->next =
		(struct LinkedList*)malloc(sizeof(struct LinkedList));
	if(((struct LinkedList*) (l->f_el))->next == NULL)
		return -1;
	l->f_el = ((struct LinkedList*) (l->f_el))->next;
	((struct LinkedList*) (l->f_el))->value = NULL;
	((struct LinkedList*) (l->f_el))->next = NULL;
	l->size++;
	return 0;
}
void* ll_remove(List* l, void* value){
	Iterator* i;
	void* val = ((struct LinkedList*)l->list)->value;
	struct LinkedList* tll;
	if(val == value){
		tll = l->list;
		l->list = tll->next;
		free(tll);
		l->size--;
		return val;
	}
	for(i = l->getIterator(l); i;i = i->next(i) ){
		val = i->getValue(i);
		if(value == val){
			tll->next = ((struct LinkedList*)i->val)->next;
			free(i->val);
			free(i);
			l->size--;
			break;
		}
		tll = (struct LinkedList*)i->val;
	}
	return val;
}
size_t ll_getSize(List* l){
	return l->size;
}
void ll_destroy(List* l, void (*destroyElemFunc)(void* value)){
	Iterator* i = l->getIterator(l);
	struct LinkedList* tll;
	if(i){
		tll = i->val;
		destroyElemFunc(i->getValue(i));
		while(i = i->next(i)){
			//printf("%d\n", tll);
			if(destroyElemFunc)
				destroyElemFunc(i->getValue(i));
			free(tll);
			tll = i->val;
		}
		//printf("%d\n", tll);
		free(tll);
	}
	free(l->f_el);
	free(l);
}
void* lli_getValue(Iterator* i){
	return ((struct LinkedList*)i->val)->value;
}
void* lli_next(Iterator* i){
	i->val = ((struct LinkedList*)i->val)->next;
	i->idx++;
	if(i->idx>=i->size){
		free(i);
		i = NULL;
	}
	return i;
}
void* lli_remove(Iterator* i){
	struct LinkedList* l =  i->val;

}
Iterator* ll_getIterator(List* l){
	if(l->size==0)
		return NULL;
	Iterator* i = (Iterator*) malloc(sizeof(Iterator));
	if(!i)
		return 0;
	i->idx = 0;
	i->size = l->getSize(l);
	i->val = l->list;
	i->getValue = lli_getValue;
	i->next = lli_next;
	return i;
}
void* ll_getIndex(List* l,size_t idx){
	Iterator* iter;
	void* value = NULL;
	size_t i;
	for(iter = l->getIterator(l), i = 0; iter&&(i!=idx); iter = iter->next(iter), i++);
	if(iter){

		value = iter->getValue(iter);
		free(iter);
	}
	return value;
}
List* newLinkedList(){
	List* l = (List*) malloc(sizeof(List));
	l->list = malloc(sizeof(struct LinkedList));
	l->f_el = l->list;
	((struct LinkedList*) (l->f_el))->next = NULL;
	((struct LinkedList*) (l->f_el))->value = NULL;
	l->size = 0;
	l->add = ll_add;
	l->remove = ll_remove;
	l->getSize = ll_getSize;
	l->getIterator = ll_getIterator;
	l->destroy = ll_destroy;
	l->getIndex = ll_getIndex;
	return l;
}
