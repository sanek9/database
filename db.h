#ifndef DB_H
#define DB_H
#include "List.h"
#include <time.h>
#include "Strings.h"
#include <stdint.h>

enum Columns{
   COLUMN_FIRST_NAME,
   COLUMN_LAST_NAME,
   COLUMN_MIDDLE_NAME,
   COLUMN_BIRTHDAY,
   COLUMN_GENDER,
   COLUMN_NATIONALITY,
   COLUMN_ROOM,
   COLUMN_PERSON,
   COLUMN_BIRTHDAY_SORT,
   COLUMN_GENDER_SORT,
   COLUMN_ITER,
   COLUMN_GROUP,
   COLUMN_FAMILY_SIZE,
   N_COLUMNS
};
typedef struct FileHeaderTable{
	//fpos_t fperson_pos;
	char verify[32];
	uint64_t fperson_pos;
	uint64_t person_num;
}FileHeaderTable;
struct DataBase{
	FILE* file;
	char* filename;
	struct FileHeaderTable mFHT;
	List* persons;

}mDataBase;
enum Gender{
	MALE_GENDER,
	FEMALE_GENDER
};

typedef struct Date{
	unsigned tm_mday;
	unsigned tm_mon;
	unsigned tm_year;
}Date;

#define MAX_BYTE_STORE 128
#define MAX_BYTE_STORE_GROUP 64
typedef struct{
	char firstName[MAX_BYTE_STORE];
	char lastName[MAX_BYTE_STORE];
	char middleName[MAX_BYTE_STORE];
	char group[MAX_BYTE_STORE_GROUP];
	enum Gender gender;
	Date birthday;
	char Nationality[MAX_BYTE_STORE];
	int familySize;
	int room;
}Person;

typedef struct{
	char group[MAX_BYTE_STORE];
	char Nationality[MAX_BYTE_STORE];
	int bydate;
	Date fbdate;
	Date lbdate;
	int bysize;
	int ffsize;
	int lfsize;
	int room;
}Filter;
extern void helloworld();
int openDataBase(char* path, int open);
void db_insertPerson(Person* p);
//void db_edit_person(Person*p);
void db_remove_person(Person*p);
int isSuitableByFilter(Person* p, Filter* f);
int isSuitableByString(Person* p, String* s);
int isOpenedDataBase();
void closeDataBase();
void saveDataBase();

void dateToStr(Date* d,char* str);

int compareByBirthday(Person *p1, Person *p2);


#endif // DB_H
