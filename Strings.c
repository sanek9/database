#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Strings.h"
#include <ctype.h>
#include "List.h"

#define STRING_BUF 16

size_t ussize(char* str){
	if(!str)
		return 0;
    char c = *str;
    size_t i=0;
    while(((c>>7-i)&0x1)&&i<8){
        i++;
    }
    return (i)?i:1;
}
size_t ucsize(Char c){
	if(c.ch[0]==0)
		return 0;
	return ussize(c.ch);
}
size_t ustrlen(char* str){
    size_t l = 0;
    while(*str){
        str+=ussize(str);
        l++;
    }
    return l;
}
size_t StringLen(String* str){
	return ustrlen(str->str);
}
size_t unexti(char* str,int i){
    return i+ussize(&str[i]);
}
Char getNullChar(){
	Char c;
	memset(&c, 0, sizeof(Char));
	return c;
}
Char ugetChar(char *s){
	Char w;

	memcpy(&w, s,ussize(s));
	return w;
}
void usetChar(char *s, Char c){
	memcpy(s, &c,ucsize(c));
}
int _ugetCode(char* s){
    int code = 0;
    char c = *s;
    int i = 0, j = 0, l = ussize(s);
    for(j = 0; j < l; j++){
        c = s[j];
        i =0;
        while((c>>7-i++)&0x1);
        while(i < 8){
//            printf("%d", (c>>(7-i))&0x1);
            code <<=1;
            code |=(c>>(7-i))&0x1;
            i++;
        }
    }
    return code;
}

char* _usetCode(char* s, int code){
    if (code < 0x80) {
        *s = (char)code;
        return s;
    }
    int i = 0, j = 0, l = 0, k =0,n;
    for(i = 26, l = 6;!(code>>i);i-=5,l--);
    j = l;
    n = l*5+1;
    for(i = 0; i < l;i++){
        s[i]=0;
        k = 7;
        while(j--){
            s[i]<<=1;
            s[i]|=1;
         //   s[i]<<=1;
            k--;
        }
        j=1;
        s[i]<<=1;
        while(k--){
            s[i]<<=1;
            s[i]|=(code>>--n)&0x1;
        }


    }

    return s;
}
int ugetCode(Char c){
	return _ugetCode(c.ch);
}
Char usetCode(int code){
	Char c = getNullChar();
	_usetCode(c.ch, code);
	return c;
}
String* newString(char* str){
	String* s = malloc(sizeof(String));
	s->r_size = strlen(str)+1;
	s->str = calloc(s->r_size, sizeof(char));
	strcpy(s->str, str);
	return s;
}
String* newStringWithSize(size_t size){
	String* s = malloc(sizeof(String));
	s->r_size = size*4;
	s->str = calloc(s->r_size, sizeof(char));
	return s;
}
void delString(String*s){
	if(s){
		free(s->str);
		free(s);
	}
}
char* _ugetIndex(char* c, size_t idx){
	size_t i;
	for(i = 0; (i < idx)&&*c; i++){
		c+= ussize(c);
	}
	if(i != idx)
		c = NULL;
	return c;
}
void uconcatChar(String* s, Char c){
	size_t len = StringLen(s);
	size_t rlen = strlen(s->str);
	if((rlen+ucsize(c))>s->r_size){
		s->str = (char*) realloc(s->str, (s->r_size+STRING_BUF)*sizeof(char));
		s->r_size+=STRING_BUF;
	}
	usetChar(&s->str[rlen], c);
}
Char ugetIndex(String* s, size_t idx){
	size_t i;
	char* c = _ugetIndex(s->str, idx);
	return ugetChar(c);
}
void usetIndex(String* s, Char c, size_t idx){
	char* ch = _ugetIndex(s->str, idx);
	if(!ch)
		return;
	size_t sc = ucsize(c);
	size_t ss = ussize(ch);
	if(sc!=ss)
		memmove(ch+sc, ch+ss, strlen(ch+ss)+1);
	usetChar(ch, c);
}

Char utoLower(Char c){
	if(ucsize(c)==1)
		c.ch[0] = tolower(c.ch[0]);
	else{
		int code = ugetCode(c);
		if((code>=0x0410)&&(code<0x0430))
			code+=0x20;
		c = usetCode(code);
	}
	return c;
}
Char utoUpper(Char c){
	if(ucsize(c)==1)
		c.ch[0] = toupper(c.ch[0]);
	else{
		int code = ugetCode(c);
		if((code>=0x0430)&&(code<0x0450))
			code-=0x20;
		c = usetCode(code);
	}
	return c;
}
int isDigit(Char c){
	int code = ugetCode(c);
	if((code>=0x30)&&(code<0x3A))
		return 1;
	else return 0;
}
int isAlpha(Char c){
	int arr[][3] = {
		{0x0041, 0x005B},
		{0x0061, 0x007B},
		{0x0410, 0x0450}
	};
	int code = ugetCode(c);
	size_t i;
	for(i = 0; i < 3; i++)
		if((code>=arr[i][0])&&(code<arr[i][1]))
			return 1;
	return 0;
}
int isAlNum(Char c){
	return (isAlpha(c))?1:isDigit(c);
}
int isPunct(Char c){
	int arr[][4] = {
		{0x0021, 0x0030},
		{0x003A, 0x0041},
		{0x005B, 0x0061},
		{0x007B, 0x007F}
	};
	int code = ugetCode(c);
	size_t i;
	for(i = 0; i < 4; i++)
		if((code>=arr[i][0])&&(code<arr[i][1]))
			return 1;
	return 0;
}

/*
	return 0, if equals
*/
int CharEquals(Char c1, Char c2){
	return ugetCode(c1)-ugetCode(c2);

}
int CharEqualsEgnoreCase(Char c1, Char c2){
	return CharEquals(utoLower(c1), utoLower(c2));

}

int _ustartWith(String* s, String* seq, int(*cfunc)(Char, Char)){
	size_t slen = StringLen(s);
	size_t qlen = StringLen(seq);
	size_t i;
	if(slen<qlen)
		return 0;
	for(i = 0;i < qlen; i++){
		if(cfunc(ugetIndex(s, i), ugetIndex(seq, i)))
			return 0;
	}
	return 1;
}
int ustartWith(String* s, String* seq){
	return _ustartWith(s, seq, CharEquals);
}
int ustartWithIgnoreCase(String* s, String* seq){
	return _ustartWith(s, seq, CharEqualsEgnoreCase);
}

/*
	return 0, if equals
*/
int _ucompareStrings(String* s1, String* s2, int(*cfunc)(Char, Char)){
	size_t l1 = StringLen(s1);
	size_t l2 = StringLen(s2);
	if(l1!=l2)
		return l1-l2;
	size_t i;
	int res;
	for(i = 0; i < l1; i++){
		if(res = cfunc(ugetIndex(s1, i), ugetIndex(s2, i)))
			return res;
	}
	return 0;
}

int ucomareStrings(String* s1, String* s2){
	return _ucompareStrings(s1, s2, CharEquals);
}
int ucomareStringsIgnoreCase(String* s1, String* s2){
	return _ucompareStrings(s1, s2, CharEqualsEgnoreCase);
}
String* usubString(String* s, size_t beginIndex, size_t endIndex){
	size_t subsize = endIndex-beginIndex;
	if(subsize<0)
		return NULL;
//	printf("sub s\n");
	String* substr = newStringWithSize(subsize);
	size_t i;
	for(i = beginIndex; i <= endIndex; i++){
		Char c = ugetIndex(s, i);
	//	printf("sumb %s\n", &c);
		uconcatChar(substr, ugetIndex(s, i));

	}
	uconcatChar(substr,getNullChar());
	return substr;
}
int uindexOfFromIndex(String* str, String* sub, size_t fromIndex){
	size_t l1 = StringLen(str);
	size_t l2 = StringLen(sub);
	size_t i, j = 0;
	for(i = fromIndex; i < l1; i++){
		if(!CharEquals(ugetIndex(str, i), ugetIndex(sub, j))){
			j++;
			if(j>=l2)
				return i-l2+1;
		}else
			j=0;

	}
	return -1;
}
int uindexOf(String* str, String* sub){
	return uindexOfFromIndex(str, sub, 0);
}
List* usplit(String* str, String* regex){
	size_t rlen = StringLen(regex);
	size_t len = StringLen(str);
	List* s = newLinkedList();
	int si=0, ei=0;
	while((ei = uindexOfFromIndex(str, regex, si))!=-1){
		if((ei-si)>0)
			s->add(s, usubString(str, si, ei-1));
		si = ei+rlen;
	}
	if(si<len)
		s->add(s, usubString(str, si, len-1));
	return s;
}
#if 0
int main()
{
//    termios t;

    char w[500];

//   int*e = 0;
    int i = 0,j;
    int d;
    char* s;
    char c[6];
    char c2[6];
    wchar_t c3;
    usetCode(&c3, 1045);;
    printf("%s %d\n",&c3, sizeof(c3));

    while(1){
        gets(w);
       // d = ugetCode(w);

        printf("%s %d\n",w, ustrlen(w));
        s = w;
        for(s = w; *s!=0; s+=ussize(s)){

            memset(c2, 0, 6);
            memcpy(&c3, s, ussize(s));
            usetCode(c2, ugetCode(s)+1);
            printf("%c %s %d\n",c3, c2, ugetCode(s));
        }
        j = strlen(w);
  /*      printf("------\n");
        for(i = 0; i < j; i++){

            printf("%c\n", w[i]);
        }*/
      //  memset(w,0,500);
       // printf("%d\n", d);
     //   printf("%s\n", usetCode(w, d));

    }
 //   wprintf(L"Hello world! %ls\n", w);
    return 0;
}
#endif
