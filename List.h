#ifndef LIST_H
#define LIST_H
#include <stdio.h>

typedef struct Iterator{
	void* val;
	size_t size;
	size_t idx;
	struct Iterator* (*next)(struct Iterator*);
	void* (*getValue)(struct Iterator*);
}Iterator;
typedef struct List{
	void* list;
	void* f_el;
	size_t size;
	int (*add)(struct List* l, void* value);
	void* (*remove)(struct List* l, void* value);
	void* (*getIndex)(struct List* l, size_t idx);
	size_t (*getSize)(struct List* l);
	Iterator* (*getIterator)(struct List* l);
	void (*destroy)(struct List* l, void (*destroyElemFunc)(void* value));
} List;

struct LinkedList{
	void* value;
	struct LinkedList* next;
};

#endif
