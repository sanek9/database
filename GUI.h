#ifndef GUI_H
#define GUI_H

#include <gtk/gtk.h>
#include <time.h>
#include "db.h" //Person



struct DateSelector{
	GtkComboBox* dayCB;
	GtkComboBox* monthCB;
	GtkComboBox* yearCB;
};
typedef struct AddPersonDialog{
	GtkDialog* dialog;
	GtkEntry* firstNameEntry;
	GtkEntry* lastNameEntry;
	GtkEntry* middleNameEntry;
	GtkEntry* groupEntry;
	GtkEntry* familySizeEntry;
	GtkEntry* nationalityEntry;
	struct DateSelector birthday;
	GtkComboBox* genderCBox;
	GtkEntry* roomEntry;
	GtkButton* clear_button;

}AddPersonDialog;

typedef struct FilterDialog{
	GtkDialog* dialog;
	GtkEntry* groupEntry;
	GtkEntry* roomEntry;
	GtkEntry* nationalityEntry;
	struct DateSelector fbdate;
	struct DateSelector lbdate;
	GtkCheckButton* fbdateCButton;
	GtkCheckButton* lbdateCButton;


	GtkEntry* ffSizeEntry;
	GtkEntry* lfSizeEntry;
	GtkCheckButton* ffSizeButton;
	GtkCheckButton* lfSizeButton;

}FilterDialog;

struct FilterField{
	GtkListStore* filterStore;
	GtkComboBox* CBox;
	GtkButton* remButton;
	GtkButton* addButton;
	Filter* selectedFilter;
};
struct GUI
{
    GtkWindow      *topWindow;
    struct AddPersonDialog  addPersonDialog;
    struct FilterDialog filterDialog;
    struct FilterField filterField;
    GtkTreeModelFilter* filterModel;
    GtkTreeModelSort* sortModel;
    GtkTreeView    *treeview;
    GtkListStore   *personStore;
    GtkEntry       *entryName;
    GtkActionGroup  *actiongroup;
    GtkActionGroup	*actiongroup2;
    GtkSpinner  *spinner;
    GtkLabel *message;
    GtkListStore* MonthStore;
} mainGUI;

struct SignalsTable{
	char* name;
	char* signal;
	void (*func)(GtkWidget*,gpointer);
};
enum Action{
	ACTION_OPEN,
	ACTION_EDIT,
	ACTION_INSERT,
	ACTION_REMOVE,
	ACTION_SEARCH

};

struct Month{
	char name[30];
	int days;
};
struct Month Months[12];
#define GENDERS_NUM 2
struct GenderSTR{
	char name[30];
	char short_name[15];
};
struct GenderSTR GenderSTR[GENDERS_NUM];
enum Messages{
	MESSAGE_REMOVE_QUESTION,
	MESSAGE_OPEN_FILE_ERR,
	MESSAGE_QUIT,
	MESSAGES_NUM

};
char* MessagesStore[MESSAGES_NUM];

void on_action_edit_activate(GtkWidget *button, gpointer data);
void on_action_insert_activate(GtkWidget *button, gpointer data);
void on_action_remove_activate(GtkWidget *button, gpointer data);
void on_action_create_activate(GtkWidget *button, gpointer data);
void on_action_open_activate(GtkWidget *button, gpointer data);
void on_action_close_activate(GtkWidget *button, gpointer data);
void refresh_table();

void dateSelector_getDate(struct DateSelector* ds, Date* date);
void dateSelector_setDate(struct DateSelector* ds, Date* date);
void dateSelector_setSensitive(struct DateSelector* ds, gboolean s);
void init_DateSelector(struct DateSelector* ds);


void init_addPersonDialog(GtkBuilder* builder);
Person* apd_get_person(struct AddPersonDialog* apd);
int apd_edit_person(struct AddPersonDialog* apd, Person* p);


void init_FilterDialog(GtkBuilder* builder);
int FilterDialog_getFilter(struct FilterDialog *fd, Filter *f);

void init_FilterField(GtkBuilder* builder);

void gui_init();

typedef enum TextType{
	TEXT_TYPE_NUMBER,
	TEXT_TYPE_NAME,
	TEXT_TYPE_GROUP

}TextType;

#endif // GUI_H
