#include <gtk/gtk.h>
#include "../GUI.h"
#include <time.h>

void dateSelector_setSensitive(struct DateSelector* ds, gboolean s){
	gtk_widget_set_sensitive(ds->dayCB, s);
	gtk_widget_set_sensitive(ds->monthCB, s);
	gtk_widget_set_sensitive(ds->yearCB, s);
}

void dateSelector_setDate(struct DateSelector* ds, Date* date){
//	printf("%d %d %d\n", date->tm_mday, date->tm_mon, date->tm_year);
	gtk_combo_box_set_active(ds->dayCB,date->tm_mday-1);
	gtk_combo_box_set_active(ds->monthCB,date->tm_mon);
	GtkListStore* yearStore = GTK_LIST_STORE(gtk_combo_box_get_model(ds->yearCB));
	//gtk_combo_box_set_active(ds->yearCB, date->year);
	int year;
	int valid;
	GtkTreeIter iter;
	valid = gtk_tree_model_get_iter_first(yearStore, &iter);
	while(valid){

		gtk_tree_model_get(yearStore, &iter, 0, &year, -1);
		//printf("%d\n", year);
		if(date->tm_year==year){
			gtk_combo_box_set_active_iter (ds->yearCB, &iter);
			break;
		}
		valid = gtk_tree_model_iter_next(yearStore, &iter);
	}

}
void dateSelector_getDate(struct DateSelector* ds, Date* date){
	GtkTreeIter iter;
	unsigned val;
	//printf("ss %d\n", sizeof(*date));
	GtkListStore* monthStore = GTK_LIST_STORE(gtk_combo_box_get_model(ds->monthCB));
	GtkListStore* dayStore = GTK_LIST_STORE(gtk_combo_box_get_model(ds->dayCB));
	GtkListStore* yearStore = GTK_LIST_STORE(gtk_combo_box_get_model(ds->yearCB));

	gtk_combo_box_get_active_iter(ds->dayCB, &iter);
	gtk_tree_model_get(dayStore,&iter, 0, &date->tm_mday, -1);
//	date->tm_mday = val;
	//date->day = gtk_combo_box_get_active(ds->dayCB)+1;
	date->tm_mon = gtk_combo_box_get_active(ds->monthCB);

	gtk_combo_box_get_active_iter(ds->yearCB, &iter);

	gtk_tree_model_get(yearStore,&iter, 0, &date->tm_year, -1);
	//date->tm_year= val;
//	printf("%d %d %d\n", date->tm_mday, date->tm_mon, date->tm_year);

}
void on_date_changed (GtkComboBox *widget, gpointer user_data){
	struct DateSelector* ds = user_data;
	GtkTreeIter iter;
	int i, year, days, day, month;
	GtkListStore* monthStore = GTK_LIST_STORE(gtk_combo_box_get_model(ds->monthCB));
	GtkListStore* dayStore = GTK_LIST_STORE(gtk_combo_box_get_model(ds->dayCB));
	GtkListStore* yearStore = GTK_LIST_STORE(gtk_combo_box_get_model(ds->yearCB));
//	printf( "%d\n", ds->monthCB);
	//printf( "%d\n", gtk_tree_model_get_n_columns(mm));
	gtk_combo_box_get_active_iter(ds->yearCB, &iter);
	gtk_tree_model_get(yearStore,&iter, 0, &year, -1);
//	printf("%d\n", year);

	//gtk_combo_box_get_active_iter(ds->dayCB, &iter);
	//gtk_tree_model_get(dayStore,&iter, 0, &day, -1);
	day = gtk_combo_box_get_active(ds->dayCB);

	month = gtk_combo_box_get_active(ds->monthCB);
	gtk_combo_box_get_active_iter(ds->monthCB, &iter);
	gtk_tree_model_get(monthStore,&iter, 1, &days, -1);
	gtk_list_store_clear(dayStore);

	if((!(year%4)&&(year%100)|| !(year%400))&&(month==1))
		days++;
	if(day>=days)
		day = 0;
	for(i = 1; i <= days; i++){
		gtk_list_store_append(dayStore, &iter);
		gtk_list_store_set (dayStore, &iter, 0, i, -1);
	}
	gtk_combo_box_set_active(ds->dayCB, day);
}

void init_DateSelector(struct DateSelector* ds){
//	printf( "%d\n", ds->monthCB);

	GtkTreeIter iter;
	GtkListStore* monthStore = GTK_LIST_STORE(gtk_combo_box_get_model(ds->monthCB));
	GtkListStore* dayStore = gtk_list_store_new(1, G_TYPE_INT);
	gtk_combo_box_set_model(ds->dayCB, dayStore); //GTK_LIST_STORE(gtk_combo_box_get_model(ds->dayCB));
	GtkListStore* yearStore = gtk_list_store_new(1, G_TYPE_INT);//GTK_LIST_STORE(gtk_combo_box_get_model(ds->yearCB));
	gtk_combo_box_set_model(ds->yearCB, yearStore);
	size_t i;
	for(i = 2015; i >= 1970; i--){
		gtk_list_store_append(yearStore, &iter);
		gtk_list_store_set (yearStore, &iter, 0, i, -1);
	}
	gtk_combo_box_set_active(ds->yearCB, 0);
	gtk_combo_box_get_active_iter(ds->monthCB, &iter);
	int days;
	gtk_tree_model_get(gtk_combo_box_get_model(ds->monthCB),&iter, 1, &days, -1);
	for(i = 1; i <= days; i++){
		gtk_list_store_append(dayStore, &iter);
		gtk_list_store_set (dayStore, &iter, 0, i, -1);
	}
	gtk_combo_box_set_active(ds->dayCB, 0);
	//g_signal_connect(ds->yearCB, "changed", G_CALLBACK(on_month_changed), ds);
	//printf("fsd\n");

	g_signal_connect(ds->monthCB, "changed", G_CALLBACK(on_date_changed), ds);
	g_signal_connect(ds->yearCB, "changed", G_CALLBACK(on_date_changed), ds);
}
