#include "../GUI.h"
#include "../db.h"
#include <stdio.h>
#include <string.h>
#include "../Strings.h"


int _checkData(struct AddPersonDialog* apd){
	int l = 0;
	l+=1&&gtk_entry_get_text_length(apd->familySizeEntry);
	l+=1&&gtk_entry_get_text_length(apd->firstNameEntry);
	l+=1&&gtk_entry_get_text_length(apd->lastNameEntry);
	l+=1&&gtk_entry_get_text_length(apd->middleNameEntry);
	l+=1&&gtk_entry_get_text_length(apd->nationalityEntry);
	l+=1&&gtk_entry_get_text_length(apd->groupEntry);
	l+=1&&gtk_entry_get_text_length(apd->roomEntry);
	l+=1&&gtk_entry_get_text_length(apd->familySizeEntry);
	if(l<8)
		return -1;
	return 0;
}

void _clear_fields (GtkButton *widget, gpointer   user_data){
	struct AddPersonDialog* apd = (struct AddPersonDialog*)user_data;
	gtk_entry_set_text (apd->familySizeEntry, "");
	gtk_entry_set_text (apd->firstNameEntry, "");
	gtk_entry_set_text (apd->lastNameEntry, "");
	gtk_entry_set_text (apd->middleNameEntry, "");
	gtk_entry_set_text (apd->nationalityEntry, "");
	gtk_entry_set_text (apd->groupEntry, "");
	gtk_entry_set_text (apd->roomEntry, "");
}
void _writePersonInDialog(struct AddPersonDialog* apd, Person* p){
	gtk_entry_set_text(apd->firstNameEntry, p->firstName);
	gtk_entry_set_text(apd->lastNameEntry, p->lastName);
	gtk_entry_set_text(apd->middleNameEntry, p->middleName);
//	printf("dsfe\n");
	gtk_entry_set_text(apd->nationalityEntry, p->Nationality);
	gtk_entry_set_text(apd->groupEntry, p->group);
	char str[20];
	sprintf(str,"%d",p->room);
	gtk_entry_set_text(apd->roomEntry, str);
	sprintf(str,"%d",p->familySize);
	gtk_entry_set_text(apd->familySizeEntry, str);
//	printf("dsfe\n");
	gtk_combo_box_set_active(apd->genderCBox, p->gender);
//	printf("\n%d %d %d\n", p->birthday.day, p->birthday.month, p->birthday.year);
	dateSelector_setDate(&apd->birthday, &p->birthday);
}
void _readPersonFromDialog(struct AddPersonDialog* apd, Person* p){
	dateSelector_getDate(&apd->birthday, &p->birthday);

	p->gender = gtk_combo_box_get_active(apd->genderCBox);
	strncpy(p->firstName, gtk_entry_get_text(apd->firstNameEntry), MAX_BYTE_STORE);
	strncpy(p->lastName, gtk_entry_get_text(apd->lastNameEntry), MAX_BYTE_STORE);
	strncpy(p->middleName, gtk_entry_get_text(apd->middleNameEntry), MAX_BYTE_STORE);
	strncpy(p->Nationality, gtk_entry_get_text(apd->nationalityEntry), MAX_BYTE_STORE);
	strncpy(p->group, gtk_entry_get_text(apd->groupEntry), MAX_BYTE_STORE_GROUP);
	sscanf(gtk_entry_get_text(apd->roomEntry), "%d", &p->room );
	sscanf(gtk_entry_get_text(apd->familySizeEntry), "%d", &p->familySize );
}
int _getPerson(struct AddPersonDialog* apd, Person* p){

	//p->birthday.day
	int err = 1;
	gtk_widget_set_sensitive(mainGUI.topWindow, FALSE);
	while(1){
		if(gtk_dialog_run(apd->dialog)!=GTK_RESPONSE_OK)
			break;
	//	printf("sad\n");
		if(_checkData(apd))
			continue;
		_readPersonFromDialog(apd, p);
		gtk_widget_hide(apd->dialog);
		gtk_widget_set_sensitive(mainGUI.topWindow, TRUE);
		//return p;
		return 0;
	}
	//free(p);
	gtk_widget_hide(apd->dialog);
	gtk_widget_set_sensitive(mainGUI.topWindow, TRUE);
    return -1;
}

Person* apd_get_person(struct AddPersonDialog* apd){
	Person* p = (Person*) malloc(sizeof(Person));
	if(!_getPerson(apd, p))
		return p;
	free(p);
	return NULL;
}
int apd_edit_person(struct AddPersonDialog* apd, Person* p){
	_writePersonInDialog(apd, p);
	return _getPerson(apd, p);
}

void on_tedxt_changed(GtkEditable *editable, gpointer user_data){
	int(*cfunc)(Char);
	cfunc = user_data;
	GtkEntry* entry = editable;
	gchar* str;
	str = gtk_entry_get_text(entry);
	//printf("%s\n", str);
	String *s = newString(str);
	size_t i;
	size_t len = StringLen(s);
	Char c;
	for(i = 0; i < len; i++){
		c = ugetIndex(s, i);
		if(!cfunc(c)){
			usetIndex(s, getNullChar(), i);
			i--;
			len--;
		}else{
			usetIndex(s, utoLower(c), i);
		}
	}
	if(len)
		usetIndex(s, utoUpper(ugetIndex(s, 0)), 0);

	gtk_entry_set_text(entry, s->str);
	//printf("%s\n", str);
	delString(s);
	//printf("%d\n", strlen(str));
	//g_free(str);
}
gboolean entryComplMF(GtkEntryCompletion *completion, const gchar *key,
                                GtkTreeIter *iter,
                                gpointer user_data){
	GtkListStore* store = gtk_entry_completion_get_model(completion);
	gchar* str;
	gboolean val;
	gtk_tree_model_get(store, iter, gtk_entry_completion_get_text_column(completion), &str, -1);

	String *s = newString(str);
	String *seq = newString(key);
	val = ustartWithIgnoreCase(s, seq);
//	printf("%s %s %d\n", str, key, val);
	g_free(str);
	delString(s);
	delString(seq);
	return val;

}
void init_addPersonDialog(GtkBuilder* builder){
	struct AddPersonDialog* apd = &mainGUI.addPersonDialog;
	apd->dialog = GTK_DIALOG(gtk_builder_get_object (builder, "add_person_dialog"));
	gtk_dialog_add_buttons(apd->dialog,
	 GTK_STOCK_OK, GTK_RESPONSE_OK, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);
	apd->birthday.dayCB =
		GTK_COMBO_BOX(gtk_builder_get_object (builder, "dayCB"));
	apd->birthday.monthCB =
		GTK_COMBO_BOX(gtk_builder_get_object (builder, "monthCB"));
	apd->birthday.yearCB =
		GTK_COMBO_BOX(gtk_builder_get_object (builder, "yearCB"));
	init_DateSelector(&apd->birthday);
	apd->firstNameEntry =
		GTK_ENTRY(gtk_builder_get_object (builder, "first_name_entry"));
	apd->lastNameEntry =
		GTK_ENTRY(gtk_builder_get_object (builder, "last_name_entry"));
	apd->middleNameEntry =
		GTK_ENTRY(gtk_builder_get_object (builder, "middle_name_entry"));
	apd->nationalityEntry =
		GTK_ENTRY(gtk_builder_get_object (builder, "nationality_entry"));
	apd->roomEntry =
		GTK_ENTRY(gtk_builder_get_object (builder, "room_entry"));
	apd->groupEntry =
		GTK_ENTRY(gtk_builder_get_object (builder, "group_entry"));
		apd->familySizeEntry =
		GTK_ENTRY(gtk_builder_get_object (builder, "family_size_entry"));
    apd->genderCBox =
		GTK_COMBO_BOX(gtk_builder_get_object (builder, "genderCBox"));
	apd->clear_button = GTK_BUTTON(gtk_builder_get_object (builder, "clear_button"));
	gtk_window_set_transient_for (apd->dialog,mainGUI.topWindow);
	gtk_window_set_modal (apd->dialog, TRUE);

	g_signal_connect(apd->clear_button, "clicked", G_CALLBACK(_clear_fields), apd);
	entry_handler_set(apd->firstNameEntry, TEXT_TYPE_NAME);
	entry_handler_set(apd->lastNameEntry, TEXT_TYPE_NAME);
	entry_handler_set(apd->middleNameEntry, TEXT_TYPE_NAME);
	entry_handler_set(apd->nationalityEntry, TEXT_TYPE_NAME);
	entry_handler_set(apd->roomEntry, TEXT_TYPE_NUMBER);
	entry_handler_set(apd->familySizeEntry, TEXT_TYPE_NUMBER);
	entry_handler_set(apd->groupEntry, TEXT_TYPE_GROUP);
}
