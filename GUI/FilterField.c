#include "../GUI.h"
#include "../db.h"
#include <string.h>

gboolean separator_func(GtkTreeModel *model, GtkTreeIter *iter, gpointer data){
	char* str;
	gtk_tree_model_get(model, iter, 0, &str, -1);
	if(!str)
		return TRUE;
	return FALSE;
}

void FilterToStr(Filter* f, char*str){

	//strcat(str, )
	if(*f->group){
		strcat(str, "G:");
		strcat(str, f->group);
		strcat(str, " ");
	}
	if(f->room!=-1){
		char s[20];
		sprintf(s, "%d", f->room);
		strcat(str, "R:");
		strcat(str, s);
		strcat(str, " ");
	}
	if(*f->Nationality){
		strcat(str, "N:");
		strcat(str, f->Nationality);
		strcat(str, " ");
	}
	if(f->bydate){
		char s[40]="";
		dateToStr(&f->fbdate, s);
		sprintf(str, "D:%s", s);
		if(compareDates(&f->fbdate, &f->lbdate)){
			dateToStr(&f->lbdate, s);
			strcat(str, "-");
			strcat(str, s);
		}
		strcat(str, " ");
	}
	if(f->bysize){
		char s[20];
		sprintf(s, "%d", f->ffsize);
		strcat(str, "F:");
		strcat(str, s);
		if(f->ffsize!=f->lfsize){
			sprintf(s, "-%d", f->lfsize);
			strcat(str, s);
		}
		strcat(str, " ");
	}
}
void add_filter (GtkButton *widget, gpointer   user_data){
	struct FilterField *ff = (FilterDialog*) user_data;
	Filter* f = (Filter*) malloc(sizeof(Filter));
	if(FilterDialog_getFilter(&mainGUI.filterDialog, f)){
		free(f);
		return;
	}
	GtkTreeIter iter;
	gtk_list_store_append(ff->filterStore, &iter);
	char str[200] = "";
	FilterToStr(f, str);
	gtk_list_store_set(ff->filterStore, &iter, 0, str, 1, f, -1);
	gtk_combo_box_set_active_iter(ff->CBox, &iter);

}
void rem_filter (GtkButton *widget, gpointer   user_data){
	struct FilterField *ff = (FilterDialog*) user_data;
	Filter *f;
	GtkTreeIter iter;

	if(gtk_combo_box_get_active_iter(ff->CBox, &iter)){
		gtk_tree_model_get(ff->filterStore, &iter, 1, &f, -1);
		free(f);
		gtk_combo_box_set_active(ff->CBox, 0);
		gtk_list_store_remove(ff->filterStore, &iter );
	}
}
Filter* filter_field_get_sel_Filter(struct FilterField *filterField){
	return filterField->selectedFilter;
}

void on_cmbx_changed (GtkComboBox *widget, gpointer user_data){
	struct FilterField *fd = (FilterDialog*) user_data;
	GtkTreeIter iter;
	Filter* filter;
	gtk_combo_box_get_active_iter(fd->CBox, &iter);
	gtk_tree_model_get(fd->filterStore, &iter, 1, &filter, -1);
	gtk_widget_set_sensitive(fd->remButton, filter);
	fd->selectedFilter = filter;
	refresh_table();
	//gtk_widget_set_sensitive(fd->remButton, 1);
	/*printf("%s %s %d\n", f->group, f->Nationality, f->room);
	printf("%d %d %d\n", f->fbdate.tm_mday, f->fbdate.tm_mon, f->fbdate.tm_year);
	printf("%d %d %d\n", f->lbdate.tm_mday, f->lbdate.tm_mon, f->lbdate.tm_year);
	printf("%d %d\n", f->ffsize, f->lfsize);*/
}
void init_FilterField(GtkBuilder* builder){
	struct FilterField *f = &mainGUI.filterField;

	f->CBox = GTK_COMBO_BOX(gtk_builder_get_object(builder, "filter_CBox"));
	f->addButton = GTK_BUTTON(gtk_builder_get_object(builder, "add_filter_button"));
	f->remButton = GTK_BUTTON(gtk_builder_get_object(builder, "rem_filter_button"));
	f->filterStore = GTK_LIST_STORE(gtk_builder_get_object(builder, "filterStore"));

	gtk_combo_box_set_row_separator_func(f->CBox, separator_func, NULL, NULL);
	GtkTreeIter iter;
	gtk_list_store_append(f->filterStore, &iter);
	gtk_widget_set_sensitive(f->remButton, 0);

	g_signal_connect(f->addButton, "clicked", G_CALLBACK(add_filter), f);
	g_signal_connect(f->remButton, "clicked", G_CALLBACK(rem_filter), f);
	g_signal_connect(f->CBox, "changed", G_CALLBACK(on_cmbx_changed), f);

}
