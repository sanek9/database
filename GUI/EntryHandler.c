#include "../GUI.h"
#include "../Strings.h"
#include "../db.h"


void on_text_changed(GtkEditable *editable, gpointer user_data){
	int(*cfunc)(String*s);
	cfunc = user_data;
	GtkEntry* entry = editable;
	gchar* str;
	str = gtk_entry_get_text(entry);
	//printf("%s\n", str);
	String *s = newString(str);
	cfunc(s);
	gtk_entry_set_text(entry, s->str);
	//printf("%s\n", str);
	delString(s);
	//printf("%d\n", strlen(str));
	//g_free(str);
}

void correct_number(String* s){
	size_t i;
	size_t len = StringLen(s);
	Char c;
	for(i = 0; i < len; i++){
		c = ugetIndex(s, i);
		if(!isDigit(c)){
			usetIndex(s, getNullChar(), i);
			i--;
			len--;
		}
	}
}
void correct_name(String* s){
	size_t i;
	size_t len = StringLen(s);
	Char c;
	for(i = 0; i < len; i++){
		c = ugetIndex(s, i);
		if(!isAlpha(c)){
			usetIndex(s, getNullChar(), i);
			i--;
			len--;
		}else{
			usetIndex(s, utoLower(c), i);
		}
	}
	if(len)
		usetIndex(s, utoUpper(ugetIndex(s, 0)), 0);
}
void correct_group(String* s){
	size_t i;
	size_t len = StringLen(s);
	Char c;
	for(i = 0; i < len; i++){
		c = ugetIndex(s, i);
		if(!isAlNum(c)&&!isPunct(c)){
			usetIndex(s, getNullChar(), i);
			i--;
			len--;
		}else{
			usetIndex(s, utoUpper(c), i);
		}
	}
}

void entry_handler_set(GtkEntry* entry, TextType type){
	int(*cfunc)(String*s);
	switch (type){
		case TEXT_TYPE_NAME:
			cfunc = correct_name;
			break;
		case TEXT_TYPE_GROUP:
			cfunc = correct_group;
			break;
		case TEXT_TYPE_NUMBER:
			cfunc = correct_number;
			break;
	}
	g_signal_connect(entry, "changed", G_CALLBACK(on_text_changed), (gpointer)cfunc);
}
