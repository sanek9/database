#include "../GUI.h"
#include "../db.h"
#include "../List.h"



#define SIGNAL_TABLE_SIZE 6
struct SignalsTable mSignalsTable[] = {
	{"action_edit",		"activate",	on_action_edit_activate},
	{"action_insert",	"activate",	on_action_insert_activate},
	{"action_remove", 	"activate",	on_action_remove_activate},
	{"action_create",	"activate",	on_action_create_activate},
	{"action_open",		"activate",	on_action_open_activate},
	{"action_close",	"activate",	on_action_close_activate}
};
void connect_signals(GtkBuilder* builder){
	size_t i;
	for(i = 0; i < SIGNAL_TABLE_SIZE; i++){
		//w = GTK_WIDGET (gtk_builder_get_object(builder, mSignalsTable[i].name));
		g_signal_connect(gtk_builder_get_object(builder, mSignalsTable[i].name),
		 mSignalsTable[i].signal, G_CALLBACK(mSignalsTable[i].func), NULL);
	}
}

gboolean tEqualFunc (GtkTreeModel *model, gint column, const gchar *key,
                               GtkTreeIter *iter, gpointer search_data){
	Person *p;
	gtk_tree_model_get(model, iter, COLUMN_PERSON, &p, -1);
#if 0
	if(p)
		printf("\n%s %s %s %s %d %d %d %d %d %ld\n", p->firstName, p->lastName, p->middleName, p->Nationality,
	 p->birthday.tm_mday , p->birthday.tm_mon, p->birthday.tm_year, p->room, p->gender);
#endif // 0
	String* skey = newString(key);
	int res = isSuitableByString(p, skey);
	delString(skey);
	return !res;
}
gint CompareBirthdayFunc (GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer user_data){
	Person *p1, *p2;
	gtk_tree_model_get(model, a, COLUMN_PERSON, &p1, -1);
	gtk_tree_model_get(model, b, COLUMN_PERSON, &p2, -1);
	return compareByBirthday(p1,p2);
}
gboolean filter_func (GtkTreeModel *model, GtkTreeIter *iter, gpointer data){

	Person* p;
	Filter* filter = filter_field_get_sel_Filter(&mainGUI.filterField);
	gtk_tree_model_get(model, iter, COLUMN_PERSON, &p, -1);
	if(p&&filter){
		return isSuitableByFilter(p, filter);
	}
	return TRUE;
}
void refresh_table(){
	gtk_tree_model_filter_refilter (mainGUI.filterModel);
}
long long dateToDays(Date* d){
	//int MounthTable[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int i;
	long days = 0;

	for(i = 1900; i < d->tm_year; i++){
		days+=365;
		if(!(d->tm_year%4)&&(d->tm_year%100)|| !(d->tm_year%400))
			days++;
	}
	for(i = 0; i <= d->tm_mon; i++){
		days+=Months[i].days; //MounthTable[i];
	}
	days+=d->tm_mday;
	//printf("days %ld\n", days);
	return days;
}

void _set_person_data(GtkTreeStore* store, Person* p, GtkTreeIter* iter){
	char sd[15];
	//char gn[15];
	char* gn;

	//gtk_tree_model_get(mainGUI.MonthStore,&iter, 0, &gn, -1);

	dateToStr(&p->birthday, sd);
	gtk_list_store_set(store, iter,
			COLUMN_FIRST_NAME, p->firstName,
			COLUMN_LAST_NAME, p->lastName,
			COLUMN_MIDDLE_NAME, p->middleName,
			COLUMN_BIRTHDAY, sd,
//			COLUMN_BIRTHDAY_SORT, dateToDays(&p->birthday),
			COLUMN_NATIONALITY, p->Nationality,
			COLUMN_GROUP, p->group,
			COLUMN_GENDER, GenderSTR[p->gender].name,
			COLUMN_GENDER_SORT, p->gender,
			COLUMN_ROOM, p->room,
			COLUMN_FAMILY_SIZE, p->familySize,
			COLUMN_ITER, iter,
			COLUMN_PERSON, p, -1);
}
void readPersons(){
	Iterator* i;
	GtkTreeIter* iter;
	List* l = mDataBase.persons;
	Person* p;
	gtk_list_store_clear (mainGUI.personStore);
	for(i = l->getIterator(l); i; i = i->next(i)){
		p = i->getValue(i);
		iter = malloc(sizeof(GtkTreeIter));
		gtk_list_store_append(mainGUI.personStore, iter);
		_set_person_data(mainGUI.personStore, p, iter );
		//mainGUI.personStore;
//		printf("\n%s %s %s %s %d %d %d %d %d %ld\n", p->firstName, p->lastName, p->middleName, p->Nationality,
//	 p->birthday.tm_mday , p->birthday.tm_mon, p->birthday.tm_year, p->room, p->gender);
	}
}


void getPerson(){
	//gtk_dialog_run(mainGUI.addPersonDialog.dialog);
	Person* p;
	p = apd_get_person(&mainGUI.addPersonDialog);

//	if(p)
//	printf("\n%s %s %s %s %d %d %d %d %d\n", p->firstName, p->lastName, p->middleName, p->Nationality,
//	 p->birthday.tm_mday , p->birthday.tm_mon, p->birthday.tm_year, p->room, p->gender);
	gtk_widget_hide(mainGUI.addPersonDialog.dialog);
	//gtk_widget_show_all();
	//mainGUI.addPersonDialog
}
void waitStart(){
	gtk_widget_show(GTK_WIDGET(mainGUI.spinner));
	gtk_spinner_start(mainGUI.spinner);

}
void waitStop(){
	gtk_spinner_stop(mainGUI.spinner);
	gtk_widget_hide(GTK_WIDGET(mainGUI.spinner));
}
void user_function (GtkActionGroup *action_group, GtkAction *action, gpointer user_data){
	//waitStart();
	//printf("12%s\n",gtk_action_get_name(action));
	//usleep(1000000);
	waitStop();

}
void printMessage(char* msg){
	gtk_label_set_text(mainGUI.message, msg);
}
gboolean quit_func (GtkWidget	     *widget, GdkEventAny	     *event){
    GtkDialog* d = gtk_message_dialog_new(mainGUI.topWindow, 0, GTK_MESSAGE_WARNING,
                                 GTK_BUTTONS_YES_NO, MessagesStore[MESSAGE_QUIT]);
    int res = 1;
		//if(gtk_dialog_run(mainGUI.removeDialog)==GTK_RESPONSE_YES){
    if(gtk_dialog_run(d)==GTK_RESPONSE_YES){
        if(isOpenedDataBase()){
            saveDataBase();
            closeDataBase();
        }
        gtk_main_quit();
        res = 0;
    }
    //gtk_widget_hide(mainGUI.removeDialog);
    gtk_widget_destroy(d);
    return res;
}
void _init_months(GtkListStore* store){
	int i;
	GtkTreeIter iter;
	int v;
	int days;
	char* name;
	v = gtk_tree_model_get_iter_first(store, &iter);
	for(i = 0; (i < 12)&&v; i++){
		gtk_tree_model_get(store, &iter,0, &name, 1, &days, -1);
		Months[i].days = days;
		strcpy(Months[i].name, name);
		g_free(name);
		v = gtk_tree_model_iter_next(store, &iter);
	}
}
void _init_genders(GtkListStore* store){
	int i;
	GtkTreeIter iter;
	int v;
	int days;
	char* name;
	char* short_name;
	v = gtk_tree_model_get_iter_first(store, &iter);
	for(i = 0; (i < GENDERS_NUM)&&v; i++){
		gtk_tree_model_get(store,&iter,0, &name, -1);
//		printf("%s\n", name);
		strcpy(GenderSTR[i].name, name);
		g_free(name);
		v = gtk_tree_model_iter_next(store, &iter);
	}
}
void _init_messages(GtkListStore* store){
	int i;
	GtkTreeIter iter;
	int v;
	int days;
	char* text;

	v = gtk_tree_model_get_iter_first(store, &iter);
	for(i = 0; (i < MESSAGES_NUM)&&v; i++){
		gtk_tree_model_get(store,&iter,0, &text, -1);
//		printf("%s\n", name);
		//strcpy(GenderSTR[i].name, name);
		//g_free(name);
		MessagesStore[i]=text;
		v = gtk_tree_model_iter_next(store, &iter);
	}
}
void gui_init(){

	GtkBuilder *builder;
	GError* error = NULL;
	builder = gtk_builder_new ();
	if (!gtk_builder_add_from_file (builder, "GUI.glade", &error))
	{

                g_critical ("Не могу загрузить файл: %s", error->message);
			g_error_free (error);
			return -1;
	}
	gtk_builder_connect_signals (builder, NULL);
	connect_signals(builder);
	mainGUI.topWindow = GTK_WINDOW(gtk_builder_get_object (builder, "window"));
	mainGUI.actiongroup = gtk_builder_get_object (builder, "actiongroup1");
	mainGUI.actiongroup2 = gtk_builder_get_object (builder, "actiongroup2");
	mainGUI.spinner = gtk_builder_get_object(builder, "spinner");
	mainGUI.message = gtk_builder_get_object(builder, "label1");
	mainGUI.personStore = gtk_builder_get_object(builder, "PersonStore");
	mainGUI.MonthStore = gtk_builder_get_object(builder, "MonthStore");
	mainGUI.treeview = gtk_builder_get_object(builder, "treeview1");
	init_addPersonDialog(builder);
	init_FilterDialog(builder);
	init_FilterField(builder);
	_init_genders(gtk_builder_get_object(builder, "GenderStore"));
	_init_messages(gtk_builder_get_object(builder, "MessagesStore"));
	_init_months(mainGUI.MonthStore);
	//printf("%s\n", MessagesStore[MESSAGE_OPEN_FILE_ERR]);
/*	Filter f;
	FilterDialog_getFilter(&mainGUI.filterDialog, &f);
	printf("%s %s %d\n", f.group, f.Nationality, f.room);
	printf("%d %d %d\n", f.fbdate.tm_mday, f.fbdate.tm_mon, f.fbdate.tm_year);
	printf("%d %d %d\n", f.lbdate.tm_mday, f.lbdate.tm_mon, f.lbdate.tm_year);
	printf("%d %d\n", f.ffsize, f.lfsize);*/
	mainGUI.filterModel = gtk_tree_model_filter_new(mainGUI.personStore, NULL);
	gtk_tree_model_filter_set_visible_func(mainGUI.filterModel, filter_func, NULL, NULL);
	mainGUI.sortModel = gtk_tree_model_sort_new_with_model(mainGUI.filterModel);
	gtk_tree_view_set_model(mainGUI.treeview, mainGUI.sortModel);
	gtk_tree_view_set_search_equal_func(mainGUI.treeview, tEqualFunc, NULL, NULL);
	gtk_tree_sortable_set_sort_func(mainGUI.sortModel, COLUMN_BIRTHDAY, CompareBirthdayFunc, NULL, NULL);
	//g_signal_connect(mainGUI.actiongroup, "pre-activate", G_CALLBACK(user_function), NULL);
	gtk_action_group_set_sensitive(mainGUI.actiongroup,gtk_false());
	gtk_action_group_set_sensitive(mainGUI.actiongroup2,gtk_true());
	gtk_action_group_set_visible(mainGUI.actiongroup,gtk_false());
	gtk_action_group_set_visible(mainGUI.actiongroup2,gtk_true());

	g_signal_connect(G_OBJECT(mainGUI.topWindow), "delete_event", G_CALLBACK(quit_func), NULL);

	gtk_widget_show(mainGUI.topWindow);

}

void on_action_edit_activate(GtkWidget *button, gpointer data){
	//mDataBase.persons->getIndex(mDataBase.persons, 0);


	GtkTreeSelection* sel = gtk_tree_view_get_selection(mainGUI.treeview);
	GtkListStore* store = NULL;//mainGUI.personStore; //GTK_LIST_STORE(gtk_tree_view_get_model(mainGUI.treeview));
	GtkTreeIter iter;
	GtkTreeIter* it;
	Person* p;
	if(gtk_tree_selection_get_selected(sel, &store, &iter)){
		//printf("sd\n");
		gtk_tree_model_get(store, &iter, COLUMN_PERSON, &p, COLUMN_ITER, &it, -1);
		//printf("sdd\n");
		apd_edit_person(&mainGUI.addPersonDialog, p);
		_set_person_data(mainGUI.personStore, p, it );
	}
	//

	//
}

void on_action_insert_activate(GtkWidget *button, gpointer data){
	//getPerson();
	GtkTreeIter* iter;
	Person* p;
	p = apd_get_person(&mainGUI.addPersonDialog);
	if(p){
		iter = malloc(sizeof(GtkTreeIter));
		db_insertPerson(p);
		gtk_list_store_append(mainGUI.personStore, iter);
		_set_person_data(mainGUI.personStore, p, iter );
	}
}

void on_action_remove_activate(GtkWidget *button, gpointer data){
	GtkTreeSelection* sel = gtk_tree_view_get_selection(mainGUI.treeview);
	GtkListStore* store = NULL;//mainGUI.personStore; //GTK_LIST_STORE(gtk_tree_view_get_model(mainGUI.treeview));
	GtkTreeIter iter;
	GtkTreeIter* it;
	Person* p;

	if(gtk_tree_selection_get_selected(sel, &store, &iter)){
		//gtk_widget_set_sensitive(mainGUI.topWindow, FALSE);
		GtkDialog* d = gtk_message_dialog_new(mainGUI.topWindow, 0, GTK_MESSAGE_WARNING,
                                 GTK_BUTTONS_YES_NO, MessagesStore[MESSAGE_REMOVE_QUESTION]);

		//if(gtk_dialog_run(mainGUI.removeDialog)==GTK_RESPONSE_YES){
		if(gtk_dialog_run(d)==GTK_RESPONSE_YES){
			gtk_tree_model_get(store, &iter, COLUMN_PERSON, &p, COLUMN_ITER, &it, -1);
			db_remove_person(p);//
			gtk_list_store_remove (mainGUI.personStore, it);
			free(it);
		}
		//gtk_widget_hide(mainGUI.removeDialog);
		gtk_widget_destroy(d);
		//gtk_widget_set_sensitive(mainGUI.topWindow, TRUE);
	}
}
void on_action_close_activate(GtkWidget *button, gpointer data){
	if(isOpenedDataBase()){
		saveDataBase();
		closeDataBase();
		gtk_list_store_clear (mainGUI.personStore);
		gtk_action_group_set_sensitive(mainGUI.actiongroup,gtk_false());
		gtk_action_group_set_sensitive(mainGUI.actiongroup2,gtk_true());
		gtk_action_group_set_visible(mainGUI.actiongroup,gtk_false());
		gtk_action_group_set_visible(mainGUI.actiongroup2,gtk_true());
	}
	printMessage("");
}
void on_action_create_activate(GtkWidget *button, gpointer data){
	if(isOpenedDataBase())
		return;
	GtkWidget *dialog;
	gtk_widget_set_sensitive(mainGUI.topWindow, FALSE);
	dialog = gtk_file_chooser_dialog_new ("Create Data Base",
									  mainGUI.topWindow,
									  GTK_FILE_CHOOSER_ACTION_SAVE,
									  GTK_STOCK_CANCEL ,//_("_Cancel"),
									  GTK_RESPONSE_CANCEL,
									  GTK_STOCK_SAVE,
									  GTK_RESPONSE_ACCEPT,
									  NULL);
	GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
	gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

	gint res = gtk_dialog_run (GTK_DIALOG (dialog));
	int r = 1;
	if (res == GTK_RESPONSE_ACCEPT){
		char *filename;
		filename = gtk_file_chooser_get_filename (chooser);
		if(r = openDataBase(filename,0)){
			//r = 0;
			GtkDialog* d = gtk_message_dialog_new(mainGUI.topWindow, 0, GTK_MESSAGE_ERROR,
                                 GTK_BUTTONS_CLOSE, MessagesStore[MESSAGE_OPEN_FILE_ERR], r);
			gtk_dialog_run(d);
			gtk_widget_destroy(d);
		}else printMessage(filename);
		g_free (filename);
	}
	gtk_widget_destroy (dialog);
	gtk_widget_set_sensitive(mainGUI.topWindow, TRUE);
	if(!r){
		readPersons();
	}else{
		gtk_list_store_clear (mainGUI.personStore);
	}
	gtk_action_group_set_sensitive(mainGUI.actiongroup,!r);
	gtk_action_group_set_sensitive(mainGUI.actiongroup2,r);
	gtk_action_group_set_visible(mainGUI.actiongroup,!r);
	gtk_action_group_set_visible(mainGUI.actiongroup2,r);

}
void on_action_open_activate(GtkWidget *button, gpointer data){

	waitStart();
	GtkWidget *dialog;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
	gint res;
	gtk_widget_set_sensitive(mainGUI.topWindow, FALSE);
	dialog = gtk_file_chooser_dialog_new ("Open Data Base",
									  mainGUI.topWindow,
									  action,
									  GTK_STOCK_CANCEL ,//_("_Cancel"),
									  GTK_RESPONSE_CANCEL,
									  GTK_STOCK_OPEN,
									  GTK_RESPONSE_ACCEPT,
									  NULL);
	gtk_window_set_transient_for (dialog,mainGUI.topWindow);
	gtk_window_set_modal (dialog, TRUE);
	res = gtk_dialog_run (GTK_DIALOG (dialog));
	int r = 1;
	if (res == GTK_RESPONSE_ACCEPT)
	  {
		char *filename;
		GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
		filename = gtk_file_chooser_get_filename (chooser);
		//printf("%s\n", filename);
		if(r = openDataBase(filename,1)){
			//r = 0;
			GtkDialog* d = gtk_message_dialog_new(mainGUI.topWindow, 0, GTK_MESSAGE_ERROR,
                                 GTK_BUTTONS_CLOSE, MessagesStore[MESSAGE_OPEN_FILE_ERR], r);
			gtk_dialog_run(d);
			gtk_widget_destroy(d);
		}else printMessage(filename);
		g_free (filename);
	  }
	waitStop();
	gtk_widget_destroy (dialog);
	gtk_widget_set_sensitive(mainGUI.topWindow, TRUE);
	if(!r){
		readPersons();
	}else{
		gtk_list_store_clear (mainGUI.personStore);
	}
	gtk_action_group_set_sensitive(mainGUI.actiongroup,!r);
	gtk_action_group_set_sensitive(mainGUI.actiongroup2,r);
	gtk_action_group_set_visible(mainGUI.actiongroup,!r);
	gtk_action_group_set_visible(mainGUI.actiongroup2,r);

}
