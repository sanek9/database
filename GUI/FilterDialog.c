#include "../GUI.h"
#include <string.h>


void on_cb_date_toggled(GtkToggleButton *togglebutton, gpointer user_data){
	struct FilterDialog* fd = (struct FilterDialog*) user_data;
	gboolean b;
	b = gtk_toggle_button_get_active(fd->fbdateCButton);
	dateSelector_setSensitive(&fd->fbdate, b);
	gtk_widget_set_sensitive(GTK_WIDGET(fd->lbdateCButton) ,b);
	dateSelector_setSensitive(&fd->lbdate, b&&gtk_toggle_button_get_active(fd->lbdateCButton));
}
void on_cb_fsize_toggled(GtkToggleButton *togglebutton, gpointer user_data){
	struct FilterDialog* fd = (struct FilterDialog*) user_data;
	gboolean b;
	b = gtk_toggle_button_get_active(fd->ffSizeButton);
	gtk_widget_set_sensitive(GTK_WIDGET(fd->ffSizeEntry) ,b);
	gtk_widget_set_sensitive(GTK_WIDGET(fd->lfSizeButton) ,b);
	gtk_widget_set_sensitive(GTK_WIDGET(fd->lfSizeEntry) ,b&&gtk_toggle_button_get_active(fd->lfSizeButton));
}

void _fd_clear(struct FilterDialog *fd){
	gtk_entry_set_text(fd->groupEntry, "");
	//gtk_entry_set_text(fd->groupEntry, "");
}
int _fdcheckData(struct FilterDialog *fd){
	int res = 0;
	res+=gtk_entry_get_text_length(fd->groupEntry);
	res+=gtk_entry_get_text_length(fd->roomEntry);
	res+=gtk_entry_get_text_length(fd->nationalityEntry);
	if(gtk_toggle_button_get_active(fd->fbdateCButton))
		res++;
	if(gtk_toggle_button_get_active(fd->ffSizeButton)&&
	(gtk_entry_get_text_length(fd->ffSizeEntry)!=0))
		res++;
	if(gtk_toggle_button_get_active(fd->ffSizeButton)){
		if(gtk_entry_get_text_length(fd->ffSizeEntry)!=0)
			res++;
		if(gtk_toggle_button_get_active(fd->lfSizeButton)&&
		(gtk_entry_get_text_length(fd->lfSizeEntry)!=0)&&res)
			res++;
	}
	return !res;
}
void _fd_read_data(struct FilterDialog *fd, Filter* f){
	char* str;
	strncpy(f->group, gtk_entry_get_text(fd->groupEntry), MAX_BYTE_STORE_GROUP);
	strncpy(f->Nationality, gtk_entry_get_text(fd->nationalityEntry), MAX_BYTE_STORE);
	if(gtk_toggle_button_get_active(fd->fbdateCButton)){
		f->bydate = 1;
		dateSelector_getDate(&fd->fbdate, &f->fbdate);
		if(gtk_toggle_button_get_active(fd->lbdateCButton))
			dateSelector_getDate(&fd->lbdate, &f->lbdate);
		else
			dateSelector_getDate(&fd->fbdate, &f->lbdate);
	}else
		f->bydate = 0;
	str = gtk_entry_get_text(fd->roomEntry);
	if(*str)
		sscanf(str, "%d", &f->room );
	else
		f->room = -1;
	str = gtk_entry_get_text(fd->ffSizeEntry);
	if(*str&&gtk_toggle_button_get_active(fd->ffSizeButton)){
		sscanf(str, "%d", &f->ffsize );
		f->bysize=1;
		if(gtk_toggle_button_get_active(fd->lfSizeButton)){
			str = gtk_entry_get_text(fd->lfSizeEntry);
			sscanf(str, "%d", &f->lfsize );
		}else
			f->lfsize = f->ffsize;
	}else
		f->bysize=0;

}
int FilterDialog_getFilter(struct FilterDialog *fd, Filter *f){

	_fd_clear(fd);
	gtk_widget_set_sensitive(mainGUI.topWindow, FALSE);
	while(1){
		if(gtk_dialog_run(fd->dialog)!=GTK_RESPONSE_OK)
			break;
		if(_fdcheckData(fd))
			continue;
		_fd_read_data(fd, f);
		gtk_widget_hide(fd->dialog);
		gtk_widget_set_sensitive(mainGUI.topWindow, TRUE);
		//return p;
		return 0;
	}
	//free(p);

	gtk_widget_hide(fd->dialog);
	gtk_widget_set_sensitive(mainGUI.topWindow, TRUE);
	return -1;
}
void init_FilterDialog(GtkBuilder* builder){
	struct FilterDialog* fd = &mainGUI.filterDialog;
	fd->dialog = GTK_DIALOG(gtk_builder_get_object (builder, "FilterDialog"));
	gtk_dialog_add_buttons(fd->dialog,
	 GTK_STOCK_OK, GTK_RESPONSE_OK, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);
	fd->groupEntry = GTK_ENTRY(gtk_builder_get_object (builder, "ft_group_entry"));
	fd->nationalityEntry = GTK_ENTRY(gtk_builder_get_object (builder, "ft_nat_entry"));
	fd->roomEntry = GTK_ENTRY(gtk_builder_get_object (builder, "fr_room_entry"));

	fd->fbdate.dayCB = GTK_COMBO_BOX(gtk_builder_get_object (builder, "ftfdayCB"));
	fd->fbdate.monthCB = GTK_COMBO_BOX(gtk_builder_get_object (builder, "ftfmonthCB"));
	fd->fbdate.yearCB = GTK_COMBO_BOX(gtk_builder_get_object (builder, "ftfyearCB"));
	init_DateSelector(&fd->fbdate);

	fd->lbdate.dayCB = GTK_COMBO_BOX(gtk_builder_get_object (builder, "ftldayCB"));
	fd->lbdate.monthCB = GTK_COMBO_BOX(gtk_builder_get_object (builder, "ftlmonthCB"));
	fd->lbdate.yearCB = GTK_COMBO_BOX(gtk_builder_get_object (builder, "ftlyearCB"));
	init_DateSelector(&fd->lbdate);
	fd->fbdateCButton = GTK_CHECK_BUTTON(gtk_builder_get_object (builder, "bdCButton"));
	fd->lbdateCButton = GTK_CHECK_BUTTON(gtk_builder_get_object (builder, "ftdCButton"));
	dateSelector_setSensitive(&fd->fbdate, FALSE);
	dateSelector_setSensitive(&fd->lbdate, FALSE);
	gtk_widget_set_sensitive(fd->lbdateCButton, FALSE);

	fd->ffSizeEntry = GTK_ENTRY(gtk_builder_get_object (builder, "frFSfentry"));
	fd->lfSizeEntry = GTK_ENTRY(gtk_builder_get_object (builder, "frFSlentry"));
	fd->ffSizeButton = GTK_CHECK_BUTTON(gtk_builder_get_object (builder, "fsCButton"));
	fd->lfSizeButton = GTK_CHECK_BUTTON(gtk_builder_get_object (builder, "frFSCButton"));
	gtk_widget_set_sensitive(fd->ffSizeEntry, FALSE);
	gtk_widget_set_sensitive(fd->lfSizeEntry, FALSE);
	gtk_widget_set_sensitive(fd->lfSizeButton, FALSE);

	g_signal_connect(fd->fbdateCButton, "toggled", G_CALLBACK(on_cb_date_toggled), fd);
	g_signal_connect(fd->lbdateCButton, "toggled", G_CALLBACK(on_cb_date_toggled), fd);
	g_signal_connect(fd->ffSizeButton, "toggled", G_CALLBACK(on_cb_fsize_toggled), fd);
	g_signal_connect(fd->lfSizeButton, "toggled", G_CALLBACK(on_cb_fsize_toggled), fd);

	gtk_toggle_button_set_active(fd->lbdateCButton, FALSE);
	gtk_toggle_button_set_active(fd->lfSizeButton, FALSE);

	entry_handler_set(fd->ffSizeEntry, TEXT_TYPE_NUMBER);
	entry_handler_set(fd->lfSizeEntry, TEXT_TYPE_NUMBER);
	entry_handler_set(fd->nationalityEntry, TEXT_TYPE_NAME);
	entry_handler_set(fd->groupEntry, TEXT_TYPE_GROUP);
	entry_handler_set(fd->roomEntry, TEXT_TYPE_NUMBER);

	gtk_window_set_transient_for (fd->dialog,mainGUI.topWindow);
	gtk_window_set_modal (fd->dialog, TRUE);
	//gtk_dialog_run(fd->dialog);
	//gtk_widget_hide(fd->dialog);

}
