#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include "db.h"
#include "GUI.h"
#include <stdarg.h>
#include "List.h"
#include "Strings.h"


int main(int argc, char* argv[])
{
	gtk_init(&argc, &argv);
	gui_init();
	gtk_main();

    //printf("Hello world!\n");
    return 0;
}
