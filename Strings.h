#ifndef STRINGS_H
#define STRINGS_H
#include "Strings.h"
typedef struct Char{
	char ch[6];
}Char;

typedef struct String{
	char* str;
	size_t r_size;

}String;

Char utoLower(Char c);
Char utoUpper(Char c);
Char getNullChar();
Char usetCode(int code);
int CharEquals(Char c1, Char c2);
int CharEqualsEgnoreCase(Char c1, Char c2);

int isAlpha(Char c);
int isDigit(Char c);
int isPunct(Char c);
int isAlNum(Char c);

String* newString(char* str);
void delString(String*s);
Char ugetIndex(String* s, size_t idx);
size_t ustrlen(char* str);
size_t StringLen(String* str);
void usetIndex(String* s, Char c, size_t idx);

int ustartWith(String* s, String* seq);
int ustartWithIgnoreCase(String* s, String* seq);
int uindexOf(String* str, String* sub);
int uindexOfFromIndex(String* str, String* sub, size_t fromIndex);


struct List* usplit(String* str, String* regex);

#endif // STRINGS_H
