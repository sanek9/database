#include <stdio.h>
#include <stdlib.h>
#include "db.h"
#include <errno.h>
#include "Strings.h"

#define VERIFY "DBCBAVCW"
void helloworld(){
	printf("hello World\n");
}
void db_insertPerson(Person* p){
	if(p)
		mDataBase.persons->add(mDataBase.persons, p);
}
void db_remove_person(Person*p){
	if(p){
		mDataBase.persons->remove(mDataBase.persons, p);
	}
}

int getFDateById(Date *date, int id){
	switch(id){
			case 0:
				return date->tm_year;
			case 1:
				return date->tm_mon;
			case 2:
				return date->tm_mday;
		}
	return 0;
}
void dateToStr(Date* d,char* str){
	sprintf(str, "%02d.%02d.%d", d->tm_mday, d->tm_mon+1, d->tm_year);
}

int compareDates(Date *d1, Date *d2){
	int f1, f2;
	int i;
	for(i = 0; i < 3; i++){
		f1 = getFDateById(d1, i);
		f2 = getFDateById(d2, i);
		if(d1!=d2){
			return  (f1-f2);
		}
	}
	return 0;
}
int compareByBirthday(Person *p1, Person *p2){
	return compareDates(&p1->birthday, &p2->birthday);
}
void compareStrFields(char* c1, char* c2, int* res){
	if(*c2){
		String* s1 = newString(c1);
		String* s2 = newString(c2);
		if(ucomareStringsIgnoreCase(s1, s2))
			(*res)++;
		delString(s1);
		delString(s2);
	}
}
int isSuitableByString(Person* p, String* s){
	List* pl = newLinkedList();
//	printf("ss %s\n", s->str);
//	printf("issssss1\n");
	String *regex = newString(" ");
	List* sl = usplit(s, regex);
//	printf("issssss2\n");
	delString(regex);

	pl->add(pl, newString(p->firstName));
	pl->add(pl, newString(p->lastName));
	pl->add(pl, newString(p->middleName));
	pl->add(pl, newString(p->group));
	pl->add(pl, newString(p->Nationality));
	char d[20];
	dateToStr(&p->birthday, d);
	pl->add(pl, newString(d));
	sprintf(d, "%d", p->familySize);
	pl->add(pl, newString(d));
	sprintf(d, "%d", p->room);
	pl->add(pl, newString(d));
    Iterator *i, *j;
    int fl = 0;
//    printf("issssss3\n");
    for(i = sl->getIterator(sl); i; i = i->next(i)){
		fl = 0;
		String *s2 = i->getValue(i);
//		printf("%s\n", s2->str);
		for(j = pl->getIterator(pl); j; j = j->next(j)){
			//printf("%s\n", s1->str);

			if(ustartWithIgnoreCase(j->getValue(j), i->getValue(i)))
				fl++;
		}
		if(!fl)
			break;
    }
 //   printf("issssss4\n");
    sl->destroy(sl, delString);

	pl->destroy(pl, delString);
	return fl;
}
int isSuitableByFilter(Person* p, Filter* f){
	int res = 0;
	compareStrFields(p->group, f->group, &res);
	compareStrFields(p->Nationality, f->Nationality, &res);
	if(res)
		return 0;
	if(f->bydate&&!((compareDates(&p->birthday, &f->fbdate)>=0)&&
	(compareDates(&p->birthday, &f->lbdate)<=0)))
		return 0;
	if((f->room!=-1)&&(p->room!=f->room))
		return 0;
	if((f->bysize)&&
		!((p->familySize>=f->ffsize)&&
		(p->familySize<=f->lfsize)))
		return 0;
	return 1;
}
FileHeaderTable getFHT(){
	FileHeaderTable fht;
	memset(&fht, 0, sizeof(FileHeaderTable));
	strcpy(fht.verify, VERIFY);
	fht.person_num = 0;
	fht.fperson_pos = 0;//sizeof(FileHeaderTable);
	return fht;
}

void updateFHT(){
	mDataBase.mFHT.person_num = mDataBase.persons->getSize(mDataBase.persons);
	//mDataBase.mFHT.fperson_pos.__pos = (sizeof(FileHeaderTable)+mDataBase.mFHT.person_num*sizeof(Person));
	mDataBase.mFHT.fperson_pos = sizeof(FileHeaderTable);//+mDataBase.mFHT.person_num*sizeof(Person));
	//printf("p num %d %ld", mDataBase.mFHT.person_num, mDataBase.mFHT.fperson_pos);
}
void writeFHT(){
	fseek(mDataBase.file, 0, SEEK_SET);
	fwrite(&mDataBase.mFHT, sizeof(FileHeaderTable), 1, mDataBase.file);
	fflush(mDataBase.file);
}
int readFHT(FILE* f){
	FileHeaderTable fht;
	fseek(f, 0, SEEK_SET);
//	printf("2\n");
	if(fread(&fht, sizeof(FileHeaderTable), 1, f)!=1)
		return -1;
//	printf("2\n");
	if(strncmp(fht.verify, VERIFY, sizeof(fht.verify)))
		return -2;
	mDataBase.mFHT = fht;
//	printf("2\n");
	return 0;
}
void readPersonsFormFile(){
	//fsetpos(mDataBase.file, &mDataBase.mFHT.fperson_pos);
	fseek(mDataBase.file, mDataBase.mFHT.fperson_pos, SEEK_SET);
	size_t len = mDataBase.mFHT.person_num;
	size_t i;
	Person* s;
	for(i = 0; i < len; i++){
		s = (Person*) malloc(sizeof(Person));
		fread(s,sizeof(Person),1,mDataBase.file);
		mDataBase.persons->add(mDataBase.persons, s);
	}
}
void writePersonsToFile(){
	//fsetpos(mDataBase.file, &mDataBase.mFHT.fperson_pos);
	fseek(mDataBase.file, mDataBase.mFHT.fperson_pos, SEEK_SET);
	Iterator* i;
	Person* p;
	List* l = mDataBase.persons;

	for(i = l->getIterator(l); i ; i = i->next(i)){
		p = i->getValue(i);
		fwrite(p, sizeof(Person), 1, mDataBase.file);
	}
}
int openDataBase(char* path, int open){
	errno = 0;
	FILE *f = NULL;
//	printf("1\n");
	if(open)
		f = fopen(path, "r+");
	else{
//	printf("1\n");
		f = fopen(path, "w+");
		if(!f)
			return errno;
		FileHeaderTable fht = getFHT();
		fwrite(&fht, sizeof(FileHeaderTable), 1, f);
		fflush(f);
	}
//	printf("Error: %s\n", strerror_r(errno));
	if(!f)
		return errno;
	//fseek(f, 0, SEEK_SET);
//	printf("1\n");
	//fread(&mDataBase.mFHT, sizeof(FileHeaderTable), 1, f);
	if(readFHT(f)){
	//	printf(" 1\n");
		fclose(f);
		return -3;
	}
//	printf("1\n");
	mDataBase.file = f;
	mDataBase.persons = newLinkedList();
	readPersonsFormFile();
	return 0;
}
int isOpenedDataBase(){
	if(mDataBase.file)
		return 1;
	return 0;
}
void closeDataBase(){
	if(isOpenedDataBase()){
		fclose(mDataBase.file);
		mDataBase.file = NULL;
		//free(mDataBase.filename)
		if(mDataBase.persons){
			mDataBase.persons->destroy(mDataBase.persons, free);
			mDataBase.persons = NULL;
		}
	}

}
void saveDataBase(){
	updateFHT();
	writeFHT();
	//printf("fht\n");
	writePersonsToFile();

}
